# Docker PHP

It is a boilerplate for legacy server configurations.

## How to use

### Apache + PHP 5.6 + MySQL 5

    $ git clone https://gitlab.com/AbeEstrada/docker-php.git
    $ cd docker-php
    $ docker-compose up -d

### Nginx + PHP 7.1 + MySQL

    $ git clone https://gitlab.com/AbeEstrada/docker-php.git
    $ cd docker-php
    $ docker-compose -f docker-compose-nginx.yml up -d

\* [PHP Supported Versions](http://php.net/supported-versions.php)
